# SOLARNET VESPA metadata crosswalk

Sources: 
- [SOLARNET FITS keywords](https://arxiv.org/pdf/2011.12139.pdf)
- [VESPA/EPNcore dictionary](https://voparis-wiki.obspm.fr/display/VES/EPN-TAP+v2+parameter+description)

## Simple mapping 

| SOLARNET | Transformation | VESPA/EPNcore | Note |
| -------- | -------------- | ------------- | ---- |
| DATE-BEG | ISO to JD      | time_min      | |
| DATE-END | ISO to JD      | time_max      | If not present, use DATE-BEG again |
| DSUN_OBS | m to AU        | target_distance_min | or use DSUN_AU if available |
| DSUN_OBS | m to AU        | target_distance_max | or use DSUN_AU if available |
| XPOSURE  | convert to sec | time_exp_min | if constant, if not use min of XPOSURE variable keyword |
| XPOSURE  | convert to sec | time_exp_max | if constant, if not use max of XPOSURE variable keyword |
| CADENCE or CADMIN | convert to sec | time_sampling_step_min | |
| CADENCE or CADMAX | convert to sec | time_sampling_step_min | |
| WAVEMIN | convert to Hz | spectral_range_max | |
| WAVEMAX | convert to Hz | spectral_range_min | |
| RESOLVPW |               | spectral_resolution_min | |
| RESOLVPW |               | spectral_resolution_max | |
| MISSION or OBSRVTRY or TELESCOP | | instrument_host_name | |
| INSTRUME | | instrument_name | |

## More complex parameters

### Product identification
- `granule_uid`: unique id (e.g., filename without extension, if is is unique)
- `granule_gid`: OBS_MODE, if available, or any human-readable name allowing to group similar observations
- `obs_id`: grouping by observation (e.g., CAMPAIGN keyword is available, or same as `granule_uid` if independent observations)
- `measurement_type`: BTYPE if already in UCD form. If not, find best fit UCD, or ask for new words.
- `processing_level`: set manually (not available in SOLARNET)

### Spatial coverage
Spatial coverage shall be reconstructed from the WCS coordinates and keywords.

## Other parameters 

- `target_name` = `Sun`
- `target_class` = `star`
- `access_format` = `application/fits`

